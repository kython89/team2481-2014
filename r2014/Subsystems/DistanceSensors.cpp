/*
 * DistanceSensors.cpp
 *
 *  Created on: Jan 30, 2014
 *      Author: Team2481
 */

#include "DistanceSensors.h"

DistanceSensors::DistanceSensors(uint32_t rightSensorChannel)
		: Subsystem("DistanceSensors"),
		rightSensor(new Ultrasonic2481(rightSensorChannel)){
	rightSensor->SetInchesPerVolt(102.041);
}

DistanceSensors::~DistanceSensors() {
	delete rightSensor;
}

float DistanceSensors::Get(){
	//float sum = rightSensor->GetDistance() + leftSensor->GetDistance();
	float sum = rightSensor->GetDistance();
	float avg = sum / 1; //2;
	return avg / 12;
}
float DistanceSensors::GetRight(){
	return rightSensor->GetDistance();
}
float DistanceSensors::GetLeft(){
	//return leftSensor->GetDistance();
	return 0.0;
}
