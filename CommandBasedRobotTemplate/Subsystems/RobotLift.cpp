/*
 * RobotLift.cpp
 *
 *  Created on: Jan 25, 2013
 *      Author: Team2481
 */

#include "RobotLift.h"
#include "../RobotMap.h"

RobotLift::RobotLift(UINT32 liftSolenoidChannel, UINT32 liftSensorChannel, UINT32 lrFlipChannel): Subsystem("RobotLift") {
	liftSolenoid = new Solenoid(liftSolenoidChannel);
	liftSensor = new DigitalInput(liftSensorChannel);
	lrRobotFlip = new Solenoid(LED_MODULE, lrFlipChannel);
}

RobotLift::~RobotLift() {
	delete liftSolenoid;
	delete liftSensor;
}
void RobotLift::lift(){
	liftSolenoid->Set(1);
}
void RobotLift::lower(){
	liftSolenoid->Set(0);
}
bool RobotLift::isLifted(){
	return liftSolenoid->Get(); //TODO change to sensor
}
void RobotLift::flip(){
	lrRobotFlip->Set(1);
}
void RobotLift::retractFlip(){
	lrRobotFlip->Set(0);
}
